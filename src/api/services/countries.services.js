import axios from 'axios'

const BASE_URL = 'https://restcountries.com/v2'

const getAllCountries = async () => {
  try {
    const response = await axios.get(BASE_URL + '/all')
    return { data: response?.data }
  } catch (error) {
    console.log('getAllCountries API ERROR: ', error)
    return { error }
  }
}

const getCountryByName = async (countryName) => {
  try {
    const response = await axios.get(`${BASE_URL}/name/${countryName}`)
    return { data: response?.data }
  } catch (error) {
    console.log('getCountry API ERROR: ', error)
    return { error }
  }
}

export { getAllCountries, getCountryByName }
