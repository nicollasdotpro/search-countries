import React, { useState, useEffect } from 'react'
import CountryCard from '../CountryCard'
import SearchBar from '../SearchBar'

import { getAllCountries, getCountryByName } from '../../api/services/countries.services'

import { Container, CountriesContainer } from './Countries.styled'

const Countries = () => {
  const [countries, setCountries] = useState([])
  const [error, setError] = useState(false)
  const [loading, setLoading] = useState(true)

  useEffect(() => {
    const cached = loadFromCache()
    if (cached?.length > 0) {
      setLoading(false)
      setCountries(cached)
    }
    else onFirstLoad()
  }, [])

  const loadFromCache = () => {
    const cached = localStorage.getItem('cachedCountries')
    const deserialized = JSON.parse(cached)
    return deserialized
  }

  const cachify = (allCountries) => localStorage.setItem('cachedCountries', JSON.stringify(allCountries))

  const onFirstLoad = async () => {
    const { data, error } = await getAllCountries()
    if (error) setError(true)
    if (data) {
      setCountries(data)
      cachify(data)
    }
    setLoading(false)
  }

  const onSearchCountry = async value => {
    setLoading(true)
    const { data, error } = await getCountryByName(value)
    if(error) return setError(true)
    if(data) setCountries(data)
    setLoading(false)
  }

  const onResetSearch = () => setCountries(loadFromCache())

  if (error) return <span>Woops something wrong happened. Reload the page.</span>

  return (
    <Container>
      <SearchBar onResetSearch={onResetSearch} onSearch={onSearchCountry} />
      {loading ? <span>loading...</span> : (
        <CountriesContainer>
          {countries?.length ? countries.map((country, index) => (
            <CountryCard key={`${country.name}-${index}`} {...country} />
          )) : <span>are you sure this country exist?</span>}
        </CountriesContainer>
      )}
    </Container>
  )
}

export default Countries
