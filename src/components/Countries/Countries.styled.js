import styled from 'styled-components'

const Container = styled.div``

const CountriesContainer = styled.div`
  display: flex;
  padding: 10px 0;
  justify-content: center;
  flex-wrap: wrap;
`

export {
  Container,
  CountriesContainer
}
