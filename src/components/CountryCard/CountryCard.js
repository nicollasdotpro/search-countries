import React, { useState } from 'react'

import {
  Container,
  Image,
  MetaDataContainer,
  MetaData,
  Label
} from './CountryCard.styled'

const CountryCard = ({ name, flag, region, capital, population }) => {
  const [showMetadata, setShowMetadata] = useState(false)
  const [fade, setFade] = useState()

  const handleExpansion = () => {
    if (showMetadata) {
      setFade('out')
      setTimeout(() => {
        setShowMetadata(false)
      }, 700)
    } else {
      setFade('in')
      setShowMetadata(true)
    }
  }

  return (
    <Container onClick={handleExpansion}>
      <Image alt={`${name}-flag`} src={flag} />
      <MetaDataContainer>
        <Label isTitle>{name}</Label>
        {showMetadata && (
          <MetaData fade={fade} open={showMetadata}>
            <Label>Region: {region}</Label>
            <Label>Capital: {capital}</Label>
            <Label>Population: {population} habitants</Label>
          </MetaData>
        )}
      </MetaDataContainer>
    </Container>
  )
}

export default CountryCard
