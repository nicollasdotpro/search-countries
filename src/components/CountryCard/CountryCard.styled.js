import styled, { css, keyframes } from 'styled-components'

const CARD_SHADOW = 'rgba(0,0,0,0.2)'

const metaIn = keyframes`
   from { opacity: 0; }
    to { opacity: 1; }
`
const metaOut = keyframes`
  from { opacity: 1; }
  to { opacity: 0; }
`

const Container = styled.div`
  display: flex;
  margin: 10px;
  width: 32vh;
  max-width: 350px;
  padding: 25px 8px;
  cursor: pointer;
  align-items: center;
  box-shadow: 0 4px 8px 0 ${CARD_SHADOW};
  transition: 0.3s;
  &:hover {
    box-shadow: 0 8px 16px 0 ${CARD_SHADOW};
  }
`

const Image = styled.img`
 width: auto;
  height: auto;
  max-width: 100px;
  max-height: 50px;
`

const MetaDataContainer = styled.div`
  display: flex;
  margin-left: 10px;
  flex-direction: column;
`

const MetaData = styled.div`
  display: flex;
  flex-direction: column;
  animation-duration: 1s;
  animation-name: ${({ fade }) => {
    if (fade === 'in') return metaIn
    if (fade === 'out') return metaOut
  }}
`

const Label = styled.span`
  font-weight: ${({ isTitle }) => isTitle ? '700' : '200'};
  font-size: ${({ isTitle }) => isTitle ? '16px' : '12px'};
`

export {
  Container,
  Image,
  MetaDataContainer,
  MetaData,
  Label
}
