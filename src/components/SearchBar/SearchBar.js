import React, { useState } from 'react'

import { Container, SearchInput, SearchButton } from './SearchBar.styled'

const SearchBar = ({ onSearch, onResetSearch }) => {
  const [searchValue, setSearchValue] = useState('')

  const handleInput = event => setSearchValue(event.target.value)

  const handleSearch = async (event) => {
    event?.preventDefault()
    if (searchValue.length === 0) onResetSearch()
    if (searchValue.length >= 3) {
      await onSearch(searchValue)
    }
  }

  const handleKeyPress = event => {
    if (event.charCode === 13) return handleSearch()
  }

  return (
    <Container>
      <SearchInput
        value={searchValue}
        onChange={handleInput}
        onKeyPress={handleKeyPress}
        placeholder='Search for a country'
       />
      <SearchButton onClick={handleSearch}>Search</SearchButton>
    </Container>
  )
}

export default SearchBar
