import styled from 'styled-components'

const Container = styled.div`
  margin: 10px 0;
`

const SearchInput = styled.input`
  padding: 7px;
  width: 30vh;
  margin: 0 5px;
  border-radius: 3px;
  border: 1px solid #ccc;
`

const SearchButton = styled.button`
  border: none;
  color: #fff;
  cursor: pointer;
  padding: 7px;
  border-radius: 3px;
  background: #000
`

export {
  Container,
  SearchInput,
  SearchButton
}
