import React from 'react';
import Countries from '../components/Countries'
import './index.css';

const Home = () => {
  return (
    <div className="App">
      <header className="App-header">
        <h1 className="App-title">Countries Of The World</h1>
      </header>
      <main>
        <Countries />
      </main>
    </div>
  );
}

export default Home;
